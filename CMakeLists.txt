cmake_minimum_required (VERSION 3.18 FATAL_ERROR)
project ("mingw64 POSIX support for Windows" VERSION 1.0.0 LANGUAGES C)

set (CMAKE_C_STANDARD 11)
set (CMAKE_C_STANDARD_REQUIRED ON)

include (FeatureSummary)
find_package (ARPA2CM 1.0.0 NO_MODULE)
set_package_properties (ARPA2CM PROPERTIES
    DESCRIPTION "CMake modules for ARPA2 projects"
    TYPE REQUIRED
    URL "https://gitlab.com/arpa2/arpa2cm/"
    PURPOSE "Required for the CMake build system for arpa2common"
)

if (ARPA2CM_FOUND)
    set (CMAKE_MODULE_PATH
        ${CMAKE_MODULE_PATH}
        ${CMAKE_SOURCE_DIR}/cmake
        ${ARPA2CM_MODULE_PATH}
    )
else()
    feature_summary (WHAT ALL)
    message (FATAL_ERROR "ARPA2CM is required.")
endif()

include (GNUInstallDirs)
include (MacroCreateConfigFiles)
include (MacroGitVersionInfo)
include (MacroLibraryPair)

get_project_git_version()

### OPTIONS / BUILD SETTINGS
#
#

option (DEBUG
    "Switch on output and flags that aid developpers in debugging"
    OFF)

option (DEBUG_DETAIL
    "Greatly expand debugging output, including packet dumps and other frivolous data"
    OFF)

if (DEBUG)
    add_compile_options (-DDEBUG -DLOG_STYLE=LOG_STYLE_STDERR -ggdb3 -O0)
endif()

if (DEBUG_DETAIL)
    add_compile_options (-DDEBUG_DETAIL)
endif()

# In debug configurations, add compile flag.
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -DDEBUG -ggdb3 -O0 -fstack-protector")

add_subdirectory(lib)
