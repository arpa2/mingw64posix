#include <io.h>
#include <fcntl.h>
#include <limits.h>

#define PIPE_BUF 512

int pipe(int pipefd[2]) {
    return _pipe(pipefd, PIPE_BUF, _O_BINARY);
}
