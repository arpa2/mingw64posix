#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <direct.h>

char *mkdtemp(char *template) {
    static char dir[256];

    char* scratch = strdup(template);
    char* start = scratch;
    if (memcmp(scratch, "/tmp/", 5) == 0) {
        start += 5;
    }
    char *end = start + strlen(start);
    while (*--end == 'X') {
    }
    end[1] = '\0';
    char *tempnam = _tempnam(NULL, start);
    if (tempnam) {
        strcpy(dir, tempnam);
        _mkdir(dir);
        free(tempnam);
        return dir;
    }
    return NULL;
}
