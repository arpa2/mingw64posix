#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

int setenv(const char *name, const char *value, int overwrite)
{
    char buf[256];
    snprintf(buf, sizeof(buf), "%s=%s", name, value);
    return _putenv (buf);
}

int unsetenv(const char *name)
{
    return setenv(name, "\"\"", 1);
}
