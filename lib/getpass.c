#include <windows.h>
#include <stdio.h>

// https://forums.codeguru.com/showthread.php?466009-Reading-from-stdin-(without-echo)
char* getpass(const char* prompt)
{
    static char password[256];

    HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
    DWORD mode = 0;
    GetConsoleMode(hStdin, &mode);
    SetConsoleMode(hStdin, mode & (~ENABLE_ECHO_INPUT));
    printf("%s", prompt);
    // https://stackoverflow.com/a/32609600/433626
    fgets(password, sizeof(password), stdin);
    /* Here remove  the trailing new line char; not required unless you are trying to print the string */
    password[strlen(password) - 1] = '\0';
    SetConsoleMode(hStdin, mode | (ENABLE_ECHO_INPUT));
    printf("\n");
    return password;
}
