#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <fcntl.h>

static int argcw;
static LPWSTR* argvw;
static char** argv2;

static void freeArgs()
{
    HANDLE hHeap = GetProcessHeap();

    for (int i = 0; i < argcw; i++) {
        HeapFree(hHeap, 0, argv2[i]);
    }
    HeapFree(hHeap, 0, argv2);
    LocalFree(argvw);
    WSACleanup();
}

int __cdecl __getmainargs(int *argc, char ***argv, char ***envp, int expand_wildcards, __UNUSED_PARAM(void *startup_info))
{
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 0), &wsaData);

    LPWSTR commandLine = GetCommandLineW();
    // Set "stdin", "stdout", and "stderr" to have binary mode:
    _setmode(_fileno(stdin), _O_BINARY);
    _setmode(_fileno(stdout), _O_BINARY);
    _setmode(_fileno(stderr), _O_BINARY);
    int rc = 0;
    argvw = CommandLineToArgvW(commandLine, &argcw);
    if (argvw) {
        HANDLE hHeap = GetProcessHeap();
        argv2 = (char**)HeapAlloc(hHeap, 0, argcw * sizeof(char*));
        for (int i = 0; i < argcw; i++) {
            int len = WideCharToMultiByte(CP_UTF8, 0, argvw[i], -1, NULL, 0, NULL, NULL);
            argv2[i] = (char*)HeapAlloc(hHeap, 0, len);
            WideCharToMultiByte(CP_UTF8, 0, argvw[i], -1, argv2[i], len, NULL, NULL);
        }
        *argc = argcw;
        *argv = argv2;
        atexit(freeArgs);
    }
    return 0;
}
