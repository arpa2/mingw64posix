#include <stdio.h>
#include <stdarg.h>
#include <io.h>

int dprintf(int fildes, const char *restrict format, ...)
{
    int written = -1;
    va_list vl;
    va_start(vl, format);
    int fd = _dup(fildes);
    if (fd != -1) {
        FILE *f = _fdopen(fd, "");
		written = vfprintf(f, format, vl);
		fclose(f);
    }
    va_end(vl);
    return written;
}
