library_pair(
    posix
    OUTPUT_NAME mingw64posix
    SOURCES
    getpass.c
    pipe.c
    mkdtemp.c
    setenv.c
    strchrnul.c
	utf8args.c
    dprintf.c
    EXPORT mingw64posix
)

install(
    DIRECTORY ../include
    DESTINATION ${CMAKE_INSTALL_PREFIX}
)
